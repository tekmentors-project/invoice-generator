let ChicagoTemplateHandlers = function () {

    const $subTotal = $('.ei-create-invoice .ei-subtotal .ei-number-lbl')[0];
    const $tax = $('.ei-create-invoice .ei-tax .ei-number-lbl')[0];
    const $total = $('.ei-create-invoice .ei-total .ei-number-lbl')[0];

    function populateView(invoiceDetails) {
        if (invoiceDetails) {
            $('.ei-phone-number').text(invoiceDetails.ownerDetails.contactNumber);
            $('.ei-email-id').text(invoiceDetails.ownerDetails.emailId);
            $('.ei-account-holder-name').text(invoiceDetails.ownerDetails.personalDetails.bankInformation.accountHolderName);
            $('.ei-bank-address').text(invoiceDetails.ownerDetails.personalDetails.bankInformation.bankName);
            $('.ei-account-number').text(invoiceDetails.ownerDetails.personalDetails.bankInformation.accountNumber);
            $('.ei-owner-name').text(invoiceDetails.ownerDetails.personalDetails.name);
            $('.ei-owner-address').text(invoiceDetails.ownerDetails.companyDetails.companyAddress);
            _populateList(invoiceDetails.itemsList);
        }
    }

    //private method
    function _populateList(data) {
        data.forEach(function (item) {
            $('.ei-item-table')
                .append('<div class="ei-item-row position-relative ei-table-row ei-table-body"><div class="ei-table-cell"><label contenteditable="true" class="ei-item-table-val ei-editable-item-description">' + item.itemDescription + '</label></div><div class="ei-table-cell"><label contenteditable="true" class="ei-item-table-val ei-editable-quantity-lbl">' + item.quantity + '</label></div><div class="ei-table-cell"><label contenteditable="true" class="ei-item-table-val ei-editable-number-lbl">' + item.price + '</label></div><div class="position-absolute ei-table-cell"><span class="ei-remove-row"><i class="fa fa-minus-square"></i></span><span class="ei-add-row"><i class="fa fa-plus-square"></i></span></div></div>');
        });
    }

    function _updateValue(){
        let subTotal = _findTotal();
        if (subTotal) {
            var taxAmnt = subTotal * 0.15;
            $subTotal.innerText = subTotal;
            $tax.innerText = taxAmnt;
            $total.innerText = subTotal + taxAmnt;
        }
    }

    function _findTotal() {
        var subTotal = 0;
        $('.ei-editable-number-lbl').each(function (key, value) {
            var pattern = /^\d+$/;
            if (pattern.test(value.innerText)) {
                subTotal = subTotal + parseInt(value.innerText);
            } else {
                console.log('Please enter numbers only');
                return false;
            }
        });
        return subTotal;
    }

    function addEventHandlers() {
        $(".ei-create-invoice").on("keyup", ".ei-editable-number-lbl", function () {
           _updateValue();
        });

        $(".ei-create-invoice").on("keyup", ".ei-editable-quantity-lbl", function () {

            var currentVal = parseInt($(event)[0].currentTarget.innerText);
            var pattern = /^\d+$/;
            if (pattern.test(currentVal)) {
                // var $adjacentUnitPrice = $($($(event)[0].currentTarget).parent().next()[0]).find('label')[0];
                // $adjacentUnitPrice.innerText = currentVal * parseInt($adjacentUnitPrice.innerText);
              //  updateValues(_findTotal());
            } else {
                console.log('Please enter numbers only');
                return false;
            }
        });

        $(".ei-create-invoice").on("click", ".ei-add-row", function () {
            $('.ei-item-table').append(`
                <div class="ei-item-row position-relative ei-table-row">
                    <div class="ei-table-cell">
                        <label contenteditable="true" class="ei-item-table-val ei-editable-item-description" data-label="Item"></label>
                    </div>
                    <div class="ei-table-cell">
                        <label contenteditable="true" class="ei-item-table-val ei-editable-quantity-lbl" data-label="1"></label>
                    </div>
                    <div class="ei-table-cell">
                        <label contenteditable="true" class="ei-item-table-val ei-editable-number-lbl" data-label="1"></label>
                    </div>
                    <div class="position-absolute ei-table-cell">
                        <span class="ei-remove-row"><i class="fa fa-minus-square"></i></span>
                        <span class="ei-add-row"><i class="fa fa-plus-square"></i></span>
                    </div>
                </div>`
            )
           _updateValue();
        });

        $(".ei-create-invoice").on("click", ".ei-remove-row", function (event) {
            $(event)[0].target.parentElement.parentElement.parentElement.remove();
        })
        $('#ei-create-invoice').on("change", "#logo", (function (e) {
            const file = e.target.files[0];
            uploadFileFirebase(file);
        }));
        // if (AppModel.user) {
        //     $(".ei-editable-item-description").autocomplete({
        //         source: getDataFromDB().getAcData,
        //         minLength: 2,
        //         response: function (event, ui) { }
        //     });
        // }
    }

    function removeEventHandlers() {

    }

    function extractInvoiceData() {
        return {
            templateID: AppModel.templateId,
            invoiceID: $('.ei-invoice-id').text(),
            ownerDetails: {
                uniqueUserid: AppModel.user,
                companyDetails: {
                    companyLogo: $('#imgLogo').attr("src") ? $('#imgLogo').attr("src") : '',
                    companyName: $('#companyName').text(),
                    companyAddress: $('.ei-owner-address').text(),
                    websiteUrl: $('#websiteUrl').text(),
                    gstIn: $('#gstin').text()
                },
                personalDetails: {
                    name: $('.ei-owner-name').text(),
                    contactNumber: $('.ei-phone-number').text(),
                    emailId: $('.ei-email-id').text(),
                    bankInformation: {
                        accountHolderName: $('.ei-account-holder-name').text(),
                        bankName: $('.ei-bank-name').text(),
                        accountNumber: $('.ei-account-number').text()
                    },
                    timestamp: new Date()
                }
            },
            customerDetails: {
                customerName: null,
                customerAddress: null
            },
            itemsList: _fetchItems(),
            subTotal: $('.ei-subtotal > .ei-number-lbl').text(),
            total: $('.ei-total > .ei-number-lbl').text()
        };
    }
    
    function _fetchItems() {
        var itemList = []; var temp_desc, temp_qty, temp_num;
        $('.ei-item-table .ei-item-row').each(function () {
            $(this).find('.ei-item-table-val').each(function () {
                if ($(this).hasClass('ei-editable-item-description')) {
                    temp_desc = $(this).text();
                }
                if ($(this).hasClass('ei-editable-quantity-lbl')) {
                    temp_qty = $(this).text();
                }
                if ($(this).hasClass('ei-editable-number-lbl')) {
                    temp_num = $(this).text();
                }
            });
            itemList.push({
                'itemDescription': temp_desc,
                'quantity': temp_qty,
                'price': temp_num
            });
        });
        return itemList;
    }

    return {
        updateView: populateView,
        addEventHandlers: addEventHandlers,
        getInvoiceModel:extractInvoiceData
    }
}