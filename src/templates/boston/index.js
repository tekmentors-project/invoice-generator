let bostonTemplateHandlers = function () {
    function populateView(invoiceDetails) {
        if (invoiceDetails) {
            $('.ei-invoice-id').text(invoiceDetails.invoiceID);
            $('.ei-phone-number').text(invoiceDetails.ownerDetails.personalDetails.contactNumber);
            $('.ei-email-id').text(invoiceDetails.ownerDetails.personalDetails.emailId);
            $('.ei-account-holder-name').text(invoiceDetails.ownerDetails.personalDetails.bankInformation.accountHolderName);
            $('.ei-bank-address').text(invoiceDetails.ownerDetails.personalDetails.bankInformation.bankName);
            $('.ei-account-number').text(invoiceDetails.ownerDetails.personalDetails.bankInformation.accountNumber);
            $('.ei-owner-name').text(invoiceDetails.ownerDetails.personalDetails.name);
            $('.ei-owner-address').text(invoiceDetails.ownerDetails.companyDetails.companyAddress);
            $('#imgLogo').attr('src', invoiceDetails.ownerDetails.companyDetails.companyLogo);
            _populateList(invoiceDetails.itemsList);
            _updateValue();
            $('[contenteditable=true]').each(function () {
                if ($(this).text() !== '')
                    $(this).addClass('filled');
                else
                    $(this).removeClass('filled');
            })
        }
    }


    //private method
    function _populateList(data) {
        $('.ei-item-table').children(".ei-table-bodygroup").remove();
        $('.ei-item-table').append('<div class="ei-table-bodygroup"></div>');
        data.forEach(function (item) {
            appendItem(item)
        });
    }

    function _updateValue() {
        const $subTotal = $('.ei-invoice-workspace .ei-subtotal .ei-number-lbl')[0];
        const $tax = $('.ei-invoice-workspace .ei-tax .ei-number-lbl')[0];
        const $total = $('.ei-invoice-workspace .ei-total .ei-number-lbl')[0];

        let subTotal = _findTotal();
        if (subTotal) {
            var taxAmnt = subTotal * 0.15;
            $subTotal.innerText = subTotal;
            $tax.innerText = taxAmnt.toFixed(2);
            $total.innerText = (subTotal + taxAmnt).toFixed(2);

            $('.total-wrap .ei-number-lbl').each(function () {
                $(this).addClass('filled');
            })
        }
    }

    function _findTotal() {
        var subTotal = 0;
        $('.ei-item-row').each(function (key, value) {
            var pattern = /^\d+$/;
            let quantity = $(this).children().children('.ei-editable-quantity-lbl');
            let itemPrice = $(this).children().children('.ei-editable-number-lbl');

            if (pattern.test($(itemPrice).text())) {
                $(itemPrice).parent().children('.alert').remove();
                let itemTotal = $(this).children().children('.ei-editable-total-lbl');
                let itemSubtotal = 0;
                    itemSubtotal = parseFloat($(quantity).text()) * parseFloat($(itemPrice).text());
                $(itemTotal).html(itemSubtotal).addClass('filled');
                subTotal = subTotal + itemSubtotal;
            } else {
                showInlineError($(itemPrice).parent(), 'Please Enter numbers Only');
            }
        });
        return subTotal;
    }

    function addEventHandlers() {
        $(".ei-invoice-workspace").on("keyup", '[contenteditable=true]', function (e) {
            if (e.target.innerText !== '')
                e.target.classList.add('filled');
            else
                e.target.classList.remove('filled');
        });
        $(".ei-invoice-workspace").on("keyup", ".ei-editable-number-lbl", function (e) {
            _updateValue();
            e.stopPropagation();
        });

        $(".ei-invoice-workspace").on("keyup", ".ei-editable-quantity-lbl", function (e) {

            var currentVal = parseInt(e.currentTarget.innerText);
            var pattern = /^\d+$/;
            if (pattern.test(currentVal)) {
                $(e.currentTarget).parent().children('.alert').remove();
            } else {
                showInlineError($(e.currentTarget).parent(), 'Please Enter numbers Only');
                return false;
            }
        });

        $(".ei-invoice-workspace .ei-add-row").click(function () {
            appendItem('');
        });

        $(".ei-invoice-workspace").on("click", ".ei-remove-row", function (event) {
            $(event)[0].target.parentElement.parentElement.parentElement.remove();
            _updateValue();
        })
        $('#ei-create-invoice').on("change", "#logo", (function (e) {
            const file = e.target.files[0];
            uploadFileFirebase(file);
        }));
    }

    function removeEventHandlers() {

    }

    function extractInvoiceData() {
        return {
            templateID: AppModel.templateId,
            invoiceID: $('.ei-invoice-id').text(),
            ownerDetails: {
                uniqueUserid: AppModel.user,
                companyDetails: {
                    companyLogo: $('#imgLogo').attr("src") ? $('#imgLogo').attr("src") : '',
                    companyName: $('#companyName').text(),
                    companyAddress: $('.ei-owner-address').text(),
                    websiteUrl: $('#websiteUrl').text(),
                    gstIn: $('#gstin').text()
                },
                personalDetails: {
                    name: $('.ei-owner-name').text(),
                    contactNumber: $('.ei-phone-number').text(),
                    emailId: $('.ei-email-id').text(),
                    bankInformation: {
                        accountHolderName: $('.ei-account-holder-name').text(),
                        bankName: $('.ei-bank-address').text(),
                        accountNumber: $('.ei-account-number').text()
                    },
                    timestamp: new Date()
                }
            },
            customerDetails: {
                customerName: null,
                customerAddress: null
            },
            itemsList: _fetchItems(),
            subTotal: $('.ei-subtotal > .ei-number-lbl').text(),
            total: $('.ei-total > .ei-number-lbl').text()
        };
    }

    function _fetchItems() {
        var itemList = []; var temp_desc, temp_qty, temp_num;
        $('.ei-item-table .ei-item-row').each(function () {
            $(this).find('.ei-item-table-val').each(function () {
                if ($(this).hasClass('ei-editable-item-description')) {
                    temp_desc = $(this).text();
                }
                if ($(this).hasClass('ei-editable-quantity-lbl')) {
                    temp_qty = $(this).text();
                }
                if ($(this).hasClass('ei-editable-number-lbl')) {
                    temp_num = $(this).text();
                }
            });
            itemList.push({
                'itemDescription': temp_desc,
                'quantity': temp_qty,
                'price': temp_num
            });
        });
        return itemList;
    }
    function showInlineError(elem, msg) {
        if($(elem).children('span').length === 0){
            $(elem).append(
                `<span class="alert text-danger">${msg}</span>`
            )
        }
    }

    function appendItem(item) {
        let className = '';
        if (item.itemDescription === '') className = 'filled';
        if (item.quantity === '') className = 'filled';
        if (item.price === '') className = 'filled';

        $('.ei-table-bodygroup')
            .append(`
        <div class="ei-item-row position-relative ei-table-row ei-table-body">
            <div class="ei-table-cell">
                <label contenteditable="true" class="ei-item-table-val ei-editable-item-description ${className}" data-label="item">${item.itemDescription ? item.itemDescription : ''}</label>
            </div>
            <div class="ei-table-cell">
                <label contenteditable="true" class="ei-item-table-val ei-editable-quantity-lbl ${className}" data-label="0">${item.quantity ? item.quantity : ''}</label>
            </div>
            <div class="ei-table-cell">
                <label contenteditable="true" class="ei-item-table-val ei-editable-number-lbl" data-label="0">${item.price ? item.price : 0}</label>
            </div>
            <div class="ei-table-cell">
                <label class="ei-item-table-val ei-editable-total-lbl">${item.price * item.quantity ? item.price * item.quantity : 0}</label>
            </div>
            <div class="ei-table-cell action-button">
                <span class="ei-remove-row">
                    <i class="fa fa-minus-square-o"></i>
                </span>
            </div>
        </div>`);
    }

    return {
        updateView: populateView,
        addEventHandlers: addEventHandlers,
        getInvoiceModel: extractInvoiceData

    }
}