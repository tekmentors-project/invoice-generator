let AppModel = {
    user: null,
    templateId: 'washington',
    invoice: getBlankInvoice(),
    userEmail : null
};

const TEMPLATES = {}
TEMPLATES['washington'] = {
    name:'washington',
    thumbnail:'images/raw/washington.png',
    html: `templates/washington/index.html`,
    script: WashingtonTemplateHandlers()
};
TEMPLATES['chicago'] = {
    name:'chicago',
    thumbnail:'images/raw/chicago.png',
    html: `templates/chicago/index.html`,
    script: WashingtonTemplateHandlers()
};
TEMPLATES['boston'] = {
    name:'boston',
    thumbnail:'images/raw/boston.png',
    html: `templates/boston/index.html`,
    script: bostonTemplateHandlers()
};
TEMPLATES['vegas'] = {
    name:'vegas',
    thumbnail:'images/raw/vegas.png',
    html: `templates/vegas/index.html`,
    script: WashingtonTemplateHandlers()
};
let activeTemplate = TEMPLATES['washington'];

function getBlankInvoice() {
    return {
        templateID: '',
        invoiceID: '',
        ownerDetails: {
            uniqueUserid: '',
            companyDetails: {
                companyLogo: 'images/raw/Placeholder.jpg',
                companyName: '',
                companyAddress: '',
                websiteUrl: '',
                gstIn: ''
            },
            personalDetails: {
                name: '',
                contactNumber: '',
                emailId: '',
                bankInformation: {
                    accountHolderName: '',
                    bankName: '',
                    accountNumber: ''
                },
                timestamp: new Date()
            }
        },
        customerDetails: {
            customerName: null,
            customerAddress: null
        },
        itemsList: [{
            itemDescription: '',
            quantity: 0,
            price: 0
        }],
        subTotal: 0,
        total: 0
    };
}