// var tid = localStorage.getItem("selectedTemplateID");
// var userID = localStorage.getItem("userId");
// var invoiceDetails = JSON.parse(localStorage.getItem('invoiceData'));
// var fromHistoryPage = localStorage.getItem('fromHistory');
$(document).ready(function () {
    localStorageLayer();
});
var localStorageLayer = function () {
    const HISTORY = false;
    const USER = 'USER';
    const INVOICE = 'INVOICE';
    const SELECTED_TEMPLATE_ID = 'SELECTED_TEMPLATE_ID';

    return {
        getSelectedTemplateId: function () {
            const templateid = localStorage.getItem(SELECTED_TEMPLATE_ID);
            if (templateid) return templateid;
            else return null;
        },
        setSelectedTemplateID: function (templateid) {
            localStorage.setItem(SELECTED_TEMPLATE_ID, templateid);
        },
        getCurrentInvoiceData: function () {
            const invoiceData = localStorage.getItem(INVOICE);
            if (invoiceData) return invoiceData;
            else return null;
        },
        setFromHistoryStatus : function() {
            localStorage.setItem(HISTORY,true);
        },
        setUserId : function(userId){
            localStorage.setItem(USER,userId);
        },
        clear : function(){
            localStorage.clear();
        }
    }

}