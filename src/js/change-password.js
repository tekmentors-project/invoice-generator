$(document).ready(function(){    
    var user = firebase.auth().currentUser;
    $('#changePassword').click(function(){      
        const user = firebase.auth().currentUser;
        let providedPassword = $('#currentPassword').val();
        let newPassword = $('#newPassword').val();
        let confirmPassword = $('#confirmPassword').val();
        const credential = firebase.auth.EmailAuthProvider.credential(user.email, providedPassword);
        //console.log(credential);
        user.reauthenticateWithCredential(credential)
            .then((_) => {
                debugger;
                if(newPassword === confirmPassword){
                    //console.log('User reauthenticated');
                    user.updatePassword(newPassword)
                    .then((_) => {
                        let errorTxt = `Password changed Successfully!`;
                        showMessage(errorTxt,'alert-success');
                        //console.log('Password changed');
                    })
                    .catch((error) => {
                        let errorTxt = `error`;
                        showMessage(errorTxt,'alert-danger');
                    })
                }else{
                    let errorTxt = `New password and confirm password isn't match`;
                    $('#confirmPassword').focus()
                    showMessage(errorTxt,'alert-danger');
                }
            })
            .catch((error) => {
                //console.log(error);
            });
    })
})