let loginRegisterModel ={
	login:{
		title:"Login"
	},
	signup:{
		title:"Register"
	}
}
let HEADER_LINKS = {};
/******************************************************/
/******************************************************/
function updateModelFromLocalStorage() {
    AppModel.templateId = localStorageLayer().getSelectedTemplateId();
    // AppModel.currentInvoice = localStorageLayer().getCurrentInvoiceData();
}
/******************************************************/
/******************************************************/
var initializeHeader = function () {
    addHeaderEventListeners();
    updateHeaderView();
}
function addHeaderEventListeners() {
    $("#signupAction").click(function (event) {
        $('.ei-sign-up-wrap').show();
        $('.ei-log-in-wrap').hide();
        $('#registerModal').modal('show');
		showModal(event);
    });
    $("#loginAction").click(function () {
        $('.ei-sign-up-wrap').hide();
        $('.ei-log-in-wrap').show();
        $('#registerModal').modal('show');
		showModal(event);
    });
    $('#logout').click(function () {
        firebase.auth().signOut();
        localStorageLayer().clear();
        window.location.href = "index.html";
    });
	$('#myProfileAction').click(function () {
		setProfileObject();
		window.history.pushState({}, "", HEADER_LINKS.profile.url);
		HEADER_LINKS.profile.updateTabView();
    });
	function showModal(event){
		let modelKey = (event.target.id == "loginAction") ? 'login' : 'signup';
		handleLoginSignupToggle(modelKey);
	}
	
}
function updateHeaderView() {
    if (AppModel.user) {
        let welcomeText = AppModel.userEmail;
        $('#regiterDiv').hide();
        if(AppModel.displayName){
            welcomeText = AppModel.displayName;
        }
        $('.ei-welcome-user span').text(welcomeText);
        $('#profileDiv').show();
        $('.ei-history-wrap').show();
    } else {
        $('#regiterDiv').show();
        $('#profileDiv').hide();
        $('.ei-history-wrap').hide();
    }
}
/******************************************************/
/******************************************************/
var initializeRightSidebar = function () {
    addRightSideBarEventListeners();
    updateRightSideView();
}
function addRightSideBarEventListeners() {
    //Save button Action
    //on saving check user is registered or not
    $("#saveData").click(function () {
        if (AppModel.user) {
            //registered user
            $('.ei-loader').show();
            AppModel.invoice = activeTemplate.script.getInvoiceModel();
            saveInvoice();
        } else {
            //new user
            $('#registerModal').modal('show');
            $('.ei-sign-up-wrap').show();
            $('.ei-log-in-wrap').hide();
            handleLoginSignupToggle("signup");
        }
    });

    //Download PDF button action
    $('#ei-savePdf').click(function () {
        exportPdf("#ei-create-invoice");
    });

    //Print button action
    $('#ei-print').click(function () {
        window.print();
    });
}
function updateRightSideView() {
}
/******************************************************/
/******************************************************/
var initializeLeftSideBar = function () {
    leftSideBarListeners();
    updateLeftSideBarView();
}
function leftSideBarListeners() {
    $(".ei-left-btn").click(function (event) {
        const tab = APPLICATION_TABS.find(tab => tab.navbarid === $(this)[0].id)
        activateTab(tab.name);
    });
}
function updateLeftSideBarView() {
    $(".ei-left-btn").removeClass('active');
    $('#' + activeTab.navbarid).addClass('active');
}
/******************************************************/
/******************************************************/
var initializeWorkspace = function () {
    APPLICATION_TABS.forEach(tab => tab.initTabsView());
    updateWorkspaceView();
}
function updateWorkspaceView() {
    $(".ei-tab-body").hide();
    $(`#${activeTab.bodyid}`).show();
    $('.ei-action-buttons').hide();
    activeTab.updateTabView();
}
/******************************************************/
/******************************************************/
function initializeBannerCta(){
    $(".ei-banner-caption button").click(function() {
        $('html, body').animate({
            scrollTop: $("#ei-dashboard").offset().top
        }, 1000);        
    });
}
function setProfileObject(){
	let profileObj = {
		bodyid: 'ei-my-profile-body',
        updateTabView: updateMyProfileBody,
        url: 'profile',
		html: `views/profile/profile.html`,
		script: myProfileHandlers()
    }
	HEADER_LINKS["profile"] = profileObj;
}
function updateMyProfileBody(){
	$(".ei-tab-body").hide();
	$(`#${HEADER_LINKS.profile.bodyid}`).show();
	$('.ei-action-buttons').hide();
	activeTab.updateTabView();
	$(".ei-create-invoice .ei-my-profile-workspace").load(HEADER_LINKS.profile.html, function () {
		HEADER_LINKS.profile.script.addEventHandlers();
		HEADER_LINKS.profile.script.getUserProfile();
	});
}
/******************************************************/
/******************************************************/
