var config = {
    apiKey: "AIzaSyCe0m-QqlcVXs7hqU7tSF7MYzxTnPSwkdY",
    authDomain: "gst-invoice-generator-53535.firebaseapp.com",
    databaseURL: "https://gst-invoice-generator-53535.firebaseio.com",
    projectId: "gst-invoice-generator-53535",
    storageBucket: "gst-invoice-generator-53535.appspot.com",
    messagingSenderId: "799816724166"
};
firebase.initializeApp(config);

// var baseURL = 'https://gst-invoice-generator-53535.firebaseio.com/invoices';
var baseURLUsers = 'https://gst-invoice-generator-53535.firebaseio.com/users';


// DBPathBuilder = function(){
//     const BASE_PATH = 'https://gst-invoice-generator-53535.firebaseio.com/users';
//     return {
//         saveInvoiceDataPath = function(){             
//              return `${BASE_PATH}/${userIdUnique}/invoiceDetails.json`
//         },
//         saveUserDetailsPath = function (userIdUnique){
//             return `/users/${userIdUnique}/userDetails`
//         },
//         fetchHistoryPath = function (userIdUnique){
//             return `${BASE_PATH}/${userIdUnique}/invoiceDetails.json`
//         },
//         userDataPath = function(userIdUnique) {
//             return `${BASE_PATH}/${userIdUnique}.json`
//         },
//         getAutoCompleteDataPath = function(userIdUnique){
//             return `${BASE_PATH}/${userIdUnique}/invoiceDetails.json`
//         }
//     }
// }

// export {DBPathBuilder}





function saveInvoiceData(data,userIdUnique) {
    return userData(baseURLUsers + `/${userIdUnique}/invoiceDetails.json`, 'post', data);
}
function saveUserDetails(data,userIdUnique) {
    return firebase.database().ref(`/users/${userIdUnique}/userDetails`).set(data); 
}
function saveUserDetailsPut(data,userIdUnique){ 
    return userData(baseURLUsers + `/${userIdUnique}/userDetails.json`, 'put', data);
}
function fetchHistory(userIdUnique) {
    return userData(baseURLUsers + `/${userIdUnique}/invoiceDetails.json`, 'get');
}
function deleteInvoiceData(key,userIdUnique){
    return userData(baseURLUsers + `/${userIdUnique}/invoiceDetails/${key}.json`, 'delete');
}
function checkIfUserDetailsExists(userIdUnique){
    return userData(baseURLUsers + `/${userIdUnique}/userDetails/uniqueUserid.json`, 'get');
}
function updateUserData(userId, data) {
    const dbPath = baseURLUsers + `/${userId}/userDetails.json`;
    return userData(dbPath, 'PUT', data);
}
function getUserData(userId) {
    const dbPath = baseURLUsers + `/${userId}.json`;
    return userData(dbPath, 'GET', '');
}
function getAutoCompleteData(userIdUnique){
    return userData(baseURLUsers + `/${userIdUnique}/invoiceDetails.json`, 'get');    
}
function userData(url, type, data) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: url,
            type: type,
            data: JSON.stringify(data),
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}
function uploadFileFirebase(fileInput) {
    $('#fileLoader').show();
    const name = (+new Date()) + '-' + fileInput.name;
    const ref = firebase.storage().ref(`logo/${name}`);
    const task = ref.put(fileInput);    
    task
    .then(snapshot => snapshot.ref.getDownloadURL())
    .then((url) => {
        //console.log(url);
        $('#fileLoader').hide();
        $('#imgLogo').attr("src",url).show();
    })
}
