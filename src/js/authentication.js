var initializeLoginSignupPopup = function () {
    addLoginSignupPopupEventListeners();
    updateLoginSignupView();
}
function addLoginSignupPopupEventListeners() {
    const auth_gst = firebase.auth();

    //change view on sign in button
    $("#signin").click(function () {
        $('.ei-sign-up-wrap').hide();
        $('.ei-log-in-wrap').show();
		handleLoginSignupToggle("login");
    });
    //change view on sign up button
    $("#signup").click(function () {
        $('.ei-sign-up-wrap').show();
        $('.ei-log-in-wrap').hide();
		handleLoginSignupToggle("signup");
    });
    //save invoice on login if data is entered
    $('#logInBtn').click(function () {
        $('.ei-loader').show();
        const username = $('#userEmail').val();
        const password = $('#userPassword').val();

        const auth_gst_login_promise = auth_gst.signInAndRetrieveDataWithEmailAndPassword(username, password);
        auth_gst_login_promise.then((data) => {
            updateLoginSignupView(data.user);
            AppModel.invoice = activeTemplate.script.getInvoiceModel();
            if(parseInt(AppModel.invoice.total) !== 0){
                saveInvoice(data);
            } else {
                $('.ei-loader').hide();
            } 
            $('#registerModal').modal('hide');
        }, (error) => {
            showMessage(error,'danger');
            $('.ei-loader').hide();
        })
    })

    //on entering sign up register the user and save invoice
    $("#signUpBtn").click(function () {
        $('.ei-loader').show();
        const username = $('#userEmail').val();
        const password = $('#userPassword').val();

        const auth_gst_signup_promise = auth_gst.createUserWithEmailAndPassword(username, password);
        auth_gst_signup_promise.then((data) => {
            updateLoginSignupView(data.user);
            AppModel.invoice = activeTemplate.script.getInvoiceModel();
            if(parseInt(AppModel.invoice.total) !== 0) {
                saveUserandInvoice(data);
            } else {
                $('.ei-loader').hide();
            } 
            $('#registerModal').modal('hide');           
        }, (error) => {
            showMessage(error,'danger');
            $('.ei-loader').hide();
        });
    });

    $("#signupActionGoogle").click(function(){
        var provider = new firebase.auth.GoogleAuthProvider();
        loginGoogleFacebook(provider);
    });

    $("#signupActionFacebook").click(function(){
        var provider = new firebase.auth.FacebookAuthProvider();
        loginGoogleFacebook(provider); 
    });

}
function loginGoogleFacebook(provider){
    firebase.auth().signInWithPopup(provider).then(function(result) {
        var token = result.credential.accessToken;            
        var user = result.user;
        updateLoginSignupView(user);
        AppModel.invoice = activeTemplate.script.getInvoiceModel();
        if(parseInt(AppModel.invoice.total) !== 0) {
            saveUserandInvoice();
        } else{
            saveUser();
            $('.ei-loader').hide();
        }
      }).catch(function(error) {            
        var errorCode = error.code;
        var errorMessage = error.message;            
        var email = error.email;            
        var credential = error.credential;
        showMessage(errorMessage,'danger');
      });
}
function updateLoginSignupView(userData) {
    if (userData) {
        userID = userData.uid;
        userEMAIL = userData.email;
        displayName = userData.displayName;
        localStorageLayer().setUserId(userID);
        AppModel.user = userID;
        AppModel.userEmail = userEMAIL;
        AppModel.displayName = displayName;
        $('#registerModal').modal('hide');
        updateHeaderView();
    }
}
function handleLoginSignupToggle(modal){
	$('#modalTitle').text(loginRegisterModel[modal].title);	
	$('.form-group').find('input').val('');
	$('#userEmail').focus();
}