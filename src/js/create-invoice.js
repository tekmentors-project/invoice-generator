const HISTORY_TAB = 'HISTORY_TAB';
const INVOICE_TAB = 'INVOICE_TAB';
const TEMPLATES_TAB = 'TEMPLATES_TAB';


let APPLICATION_TABS = [
    {
        name: INVOICE_TAB,
        navbarid: 'ei-invoice',
        bodyid: 'ei-invoice-body',
        updateTabView: updateInvoiceTabBody,
        initTabsView: initInvoiceTab,
        url: 'invoice'
    }, {
        name: HISTORY_TAB,
        navbarid: 'ei-history',
        bodyid: 'ei-history-body',
        updateTabView: updateHistoryTabBody,
        initTabsView: initHistoryTab,
        url: 'history'
    }, {
        name: TEMPLATES_TAB,
        navbarid: 'ei-templates',
        bodyid: 'ei-templates-body',
        updateTabView: updateTemplatesTabBody,
        initTabsView: initTemplatesTab,
        url: 'templates'
    }
]
let activeTab = APPLICATION_TABS.find(tab => tab.name === TEMPLATES_TAB);

$(document).ready(function () {
    const url = window.location.href;
    const lastToken = url.split('/').pop();
    const tab = APPLICATION_TABS.find(tab => tab.url === lastToken)
    if (tab) {
        activeTab = tab;
    }    
    initializeInvoicePage();
});

var initializeInvoicePage = function () {
    // updateModelFromLocalStorage();
    initializeHeader();
    initializeWorkspace();
    initializeRightSidebar();
    initializeLeftSideBar();
    initializeLoginSignupPopup();
    loadTemplate();
    initializeBannerCta();
}
$(window).on('popstate', function(event) {
    const url = window.location.href;
    const lastToken = url.split('/').pop();
    const tab = APPLICATION_TABS.find(tab => tab.url === lastToken)
    if (tab) {
        activeTab = tab;
    } 
    activateTab(tab.name);
});
/******************************************************/
/******************************************************/
function loadTemplate(tid) {
    if (TEMPLATES[tid]) {
        $(".ei-create-invoice .ei-invoice-workspace").load(TEMPLATES[tid].html, function () {
            activeTemplate = TEMPLATES[tid];
            activeTemplate.script.addEventHandlers();
            activeTemplate.script.updateView(AppModel.invoice)
        });
    }
}
function activateTab(tabName) {
    activeTab = APPLICATION_TABS.find(tab => tab.name === tabName);
    AppModel.invoice = activeTemplate.script.getInvoiceModel();
    window.history.pushState({}, "", `${activeTab.url}`);
    updateLeftSideBarView();
    updateWorkspaceView();
}
/******************************************************/
/******************************************************/
function initInvoiceTab() {
    loadTemplate(AppModel.templateId);
}
function updateInvoiceTabBody() {
    $('.ei-action-buttons').show();
    activeTemplate.script.updateView(AppModel.invoice);
}
/******************************************************/
/******************************************************/
function initHistoryTab() {
    addHistoryTabEventListeners();
    updateHistoryTabBody();
}
function addHistoryTabEventListeners() {
    $('.ei-tempate-container').on("click", $('.ei-from-history'), function (event) {
        let $card;
        let trashClicked = ($(event.target).hasClass('fa-trash') || $(event.target).hasClass('ei-history-delete-invoice'));
        if (trashClicked) {
            let key = $($(event)[0].target).parent().attr('data-key');
            if ($(event.target).hasClass('fa-trash')) {
                $card = $($($(event)[0].target).parent().parent().parent().parent());
            } else {
                $card = $($(event)[0].target).parent().parent().parent();
            }
            $('.ei-loader').show();
            deleteInvoice(key, $card);
        } else {
            if($(event.target).hasClass('ei-from-history')){
                var dataOfClickedHistoryTemplate = JSON.parse($($(event.target)[0]).attr('data-model'));
                AppModel.templateId = $($(event.target)[0]).attr('data-label');
                loadTemplate(AppModel.templateId);
                activateTab(INVOICE_TAB);
                AppModel.invoice = dataOfClickedHistoryTemplate;
                localStorageLayer().setSelectedTemplateID(AppModel.templateId);
            }
        }
    });
}
function updateHistoryTabBody() {
    if (AppModel.user) {
        $('.ei-loader').show();
        fetchHistory(userID).then((data) => {
            $('.ei-loader').hide();
            $('.ei-tempate-container .ei-template-wrap').html('');
            if(data){
                Object.keys(data).forEach(function (key, index) {
                    constructCard(data[key], key);
                });
            }            
        })
    }

    function constructCard(data, key) {
        var templateId = data.templateID;
        var createdOn = data.ownerDetails.personalDetails.timestamp;
        $('.ei-tempate-container .ei-template-wrap').prepend(`
            <div class="col-sm-3 mb-5 ei-history-card">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            <picture class="mx-auto d-block">
                                <img src="images/raw/${templateId}.png" alt="" class="img-fluid" />                                
                            </picture>
                        </h5>
                        <p class="card-text">Created on ${new Date(createdOn).toUTCString().split(' ').slice(0, 6).join(' ')}</p>
                        <a href="javascript:void(0)" class="ei-from-history btn btn-primary" data-label=${templateId} data-model='${JSON.stringify(data)}'>View Invoice</a>
                        <div class="ei-history-delete-invoice" data-key=${key}>
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>                
            </div>`)
    }
}
/******************************************************/
/******************************************************/
function initTemplatesTab() {

    Object.keys(TEMPLATES).forEach(templateKey => {
        $('#ei-templates-body').append(` <div class="col template-thumbnail-cta">
        <picture class="mx-auto d-block temaplateOverlay">
          <a href="javascript:void(0)" data-label=${TEMPLATES[templateKey].name}>
            <img src=${TEMPLATES[templateKey].thumbnail} alt="" class="img-fluid" />
          </a>
        </picture>
      </div>`);
    });
    addTemplatesTabEventLinsteners();
}
function addTemplatesTabEventLinsteners() {
    $('.template-thumbnail-cta').click(function (event) {
        AppModel.templateId = $(this).find('a').attr('data-label');
        loadTemplate(AppModel.templateId);
        activateTab(INVOICE_TAB);
    });
}
function updateTemplatesTabBody() {
    // Intentionaly left blank as there is nothing to do!!
}
/******************************************************/
/******************************************************/
/*Utility Functions*/
/******************************************************/
/******************************************************/
let exportPdf = function (template) {
    $('.ei-add-row').hide();
    kendo.drawing
        .drawDOM(template,
            {
                paperSize: "A4",
                margin: { top: "30px", bottom: "30px", right: "30px", left: "30px" },
                scale: 0.8,
                height: 500
            })
        .then(function (group) {
            kendo.drawing.pdf.saveAs(group, "Invoice.pdf")            
            $('.ei-add-row').show();
        });
}
let saveUser = function () {
    return saveUserDetails(AppModel.invoice.ownerDetails, AppModel.user).then((data) => {
        $('.ei-loader').hide();
    }, (error) => {
        showMessage(error, 'danger');
    })
}
let saveUserandInvoice = function () {
    return saveUserDetails(AppModel.invoice.ownerDetails, AppModel.user)
        .then((data) => {
            saveInvoice()
        }, (error) => {
            showMessage(error, 'danger');
        })
}
let saveInvoice = function () {
    checkIfUserDetailsExists(AppModel.user).then((value)=>{
        if(value){
            saveInvoiceData(AppModel.invoice, AppModel.user).then((data) => {
                $('.ei-loader').hide();
                showMessage('Invoice saved successfully', 'success');
            }, (error) => {
                showMessage(error, 'danger');
            })
        } else {
            saveUserDetailsPut(AppModel.invoice.ownerDetails, AppModel.user).then(()=>{
                saveInvoiceData(AppModel.invoice, AppModel.user).then((data) => {
                    $('.ei-loader').hide();
                    showMessage('Invoice saved successfully', 'success');
                }, (error) => {
                    showMessage(error, 'danger');
                });
            })
        }
    });    
}
let deleteInvoice = function (key, $card) {
    deleteInvoiceData(key, AppModel.user).then((data) => {
        $('.ei-loader').hide();
        $card.remove();
        showMessage('Invoice deleted successfully', 'danger');
    }, (error) => {
        showMessage(error, 'danger');
    });
}
function getAcData() {
    var autoCompleteArr = [];
    getAutoCompleteData(userID).then((data) => {
        Object.keys(data).forEach((item) => {
            data[item].itemsList.forEach((innerItem) => {
                autoCompleteArr.push(innerItem.itemDescription);
            });
        });
    });
    return autoCompleteArr;
}
function showMessage(text, mesageClass) {
    let messagebody = `
        <div class="alert alert-${mesageClass} alert-dismissible fade show" role="alert" style="z-index:1051;">
        ${text}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    `;
    $('body').append(messagebody);
    setTimeout(function () {
        $(".alert").alert('close');
    }, 5000)
}
/******************************************************/
/******************************************************/
