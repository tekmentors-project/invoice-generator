let myProfileHandlers = function () {
	
	var isFormValid = true;
	var isValidUserId = true;
	var userId = AppModel.user;
	
    function getUserProfile(){
		if(userId != null && userId != ''){
			getUserData(userId).then((data) => {
				if(data != null){
					populateUserDetails(data);
				}else{
					isValidUserId = false;
					$('#errorMsg').text("Please Login to View Profile Details");
				}		
			},(error) => {
				isValidUserId = false;
				$('#errorMsg').text("Error while getting user data from DB");
				console.log('Error while getting user data from DB. '+ error);
			});
		}else{
			isValidUserId = false;
			$('#errorMsg').text("Please Login to View Profile Details");
		}
	}

    function addEventHandlers() {
		$("#profileForm").keyup(function(){
			validateForm();
		});
		$("#profileForm").submit(function(){
			return saveProfileDetails();
		});
		$("#companyLogo").change(function(e){
			const file = e.target.files[0];
			var localpath = URL.createObjectURL(file);
			$('#pofileImgLogo').attr("src",localpath);
		});
	}
	
	function populateUserDetails(data) {
		var userDetailsJson = data.userDetails;
		if(userDetailsJson && userDetailsJson.companyDetails){
			$('#companyName').val(userDetailsJson.companyDetails.companyName);
			$('#companyAddress').val(userDetailsJson.companyDetails.companyAddress);
			$('#websiteUrl').val(userDetailsJson.companyDetails.websiteUrl);
			$('#gstin').val(userDetailsJson.companyDetails.gstIn);
			$('#pofileImgLogo').attr("src",userDetailsJson.companyDetails.companyLogo);
		}
		if(userDetailsJson && userDetailsJson.personalDetails){
			$('#ownerName').val(userDetailsJson.personalDetails.name);		
			$('#phone').val(userDetailsJson.personalDetails.contactNumber);
			$('#emailAddress').val(userDetailsJson.personalDetails.emailId);
			
			if(userDetailsJson.personalDetails.bankInformation){
				$('#bankName').val(userDetailsJson.personalDetails.bankInformation.bankName);
				$('#accountNumber').val(userDetailsJson.personalDetails.bankInformation.accountNumber);
				$('#accountHolderName').val(userDetailsJson.personalDetails.bankInformation.accountHolderName);	
			}
		}	
	}

	function validateForm() {
		let srcElement = getEventSource(event);
		if (srcElement) {
			validateField(srcElement);
		}
	}

	function getEventSource(event) {
		if (event.srcElement) return event.srcElement;
		else if (event.target) return event.target;
	}

	function saveProfileDetails(){	
		if(isValidUserId){
			$('#errorMsg').text("");	
			isFormValid = true;
			$('input:not([id="companyLogo"]), textarea').each(
				function(index, currentElement){ 
					validateField(currentElement);			
				}
			);
			if(isFormValid){
				$('.ei-loader').show();
				uploadLogoAndSaveJson();
			}else{
				$('span.error:not(:empty):first').attr("tabindex",-1).focus();
			}
		}else{
			jQuery("#errorMsg").attr("tabindex",-1).focus();
		}		
		return false;
	}

	function validateField(currentElement){
		if(validations[currentElement.name]){
			const validationResult = validations[currentElement.name](currentElement.value);
			if (!validationResult.result) {
				errorMsg[currentElement.name] = validationResult.msg;
				$(currentElement).addClass("is-invalid");
				isFormValid = false;
			} else {
				errorMsg[currentElement.name] = '';
				$(currentElement).removeClass("is-invalid");
			}
			const errorElement = currentElement.parentElement.querySelector('span');
			errorElement.textContent = errorMsg[currentElement.name];		
		}
	}
		
	function uploadLogoAndSaveJson() {
		let fileInput = $('#companyLogo')[0].files[0];
		if(typeof(fileInput) !== "undefined"){
			const name = (+new Date()) + '-' + fileInput.name;
			const ref = firebase.storage().ref(`logo/${name}`);
			const task = ref.put(fileInput);
			
			task
			.then(snapshot => snapshot.ref.getDownloadURL())
			.then(function(url){
				saveJson(url);
			});
		}else{
			var logoFirebaseUrl = $("#pofileImgLogo").attr('src');
			saveJson(logoFirebaseUrl);
		}
	}

	function saveJson(logoUrl) {	
		let profileJson = getFormDetailsAsJson(logoUrl);			
		updateUserData(userId, profileJson).then((data) => {
			showMessage('Profile updated successfully.', 'success');
			$('.ei-loader').hide();			
		},(error) => {			
			showMessage('Error while updating profile.', 'error');
			$('.ei-loader').hide();
		});
		
		jQuery("#errorMsg").attr("tabindex",-1).focus();
	}

	function getFormDetailsAsJson(logoUrl){	
		let profileJson = {
			companyDetails:{
				companyLogo: logoUrl,
				companyName: $('#companyName').val(),
				companyAddress: $('#companyAddress').val(),
				websiteUrl: $('#websiteUrl').val(),
				gstIn: $('#gstin').val()
			},
			personalDetails:{
				name: $('#ownerName').val(),		
				contactNumber: $('#phone').val(),
				emailId: $('#emailAddress').val(),
				bankInformation: {
					bankName: $('#bankName').val(),
					accountNumber: $('#accountNumber').val(),
					accountHolderName: $('#accountHolderName').val()
				}
			},
			uniqueUserid : AppModel.user
		}
		return profileJson;
	}

	var errorMsg = {
		ownerName: '',
		phone: '',
		emailAddress: '',
		companyName: '',
		companyAddress: '',
		websiteUrl: '',
		gstin: '',
		bankName: '',
		accountNumber: '',
		accountHolderName: ''
	}

	var validations = {
		ownerName: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Owner\'s Name'};
			} else{
				var regex = new RegExp("^[a-zA-Z0-9 \s]*$");
				if(!regex.test(value)) {
					return { result: false, msg: 'Invalid Name.' };
				}else{
					return { result: true }
				}          
			}
		},
		phone: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Contact Number'};
			} else{
				var regex = new RegExp("^[0-9 \s]*$");
				if(!regex.test(value)) {
					return { result: false, msg: 'Please enter numeric value.' };
				}else{
					return { result: true }
				}          
			}
		},
		emailAddress: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Email Address' };
			} else{
				var regex = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
				if(!regex.test(value)) {
					return { result: false, msg: 'Invalid Email Address.' };
				}else{
					return { result: true }
				}          
			}
		},
		companyName: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Company Name' };
			} else{
				var regex = new RegExp("^[a-zA-Z0-9 \s]*$");
				if(!regex.test(value)) {
					return { result: false, msg: 'Invalid Company Name' };
				}else{
					return { result: true }
				}          
			}
		},
		companyAddress: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Company Address' };
			} else{
				return { result: true }          
			}
		},
		websiteUrl: function (value) {
			var regex = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
			if (!value) {
				return { result: true};
			}else if(!regex.test(value)) {
				return { result: false, msg: 'Please enter valid URL.' };
			}else{
				return { result: true }
			}
		},
		gstin: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter GST Identification Number'};
			} else{
				var regex = new RegExp("^[a-zA-Z0-9 \s]*$");
				if(!regex.test(value)) {
					return { result: false, msg: 'GST Identification Number can be alphanumeric only.' };
				}else{
					return { result: true }
				}          
			}
		},
		bankName: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Bank Name'};
			} else{
				var regex = new RegExp("^[a-zA-Z0-9 \s]*$");
				if(!regex.test(value)) {
					return { result: false, msg: 'Invalid Bank Name.' };
				}else{
					return { result: true }
				}          
			}
		},
		accountNumber: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Account Number'};
			} else{
				var regex = new RegExp("[^0-9 \s]*$");
				if(!regex.test(value)) {
					return { result: false, msg: 'Account Number can be numeric only.' };
				}else{
					return { result: true }
				}          
			}
		},
		accountHolderName: function (value) {
			if (!value) {
				return { result: false, msg: 'Please Enter Account Holder Name'};
			} else{
				var regex = new RegExp("^[a-zA-Z0-9 \s]*$");
				if(!regex.test(value)) {
					return { result: false, msg: 'Invalid Name.' };
				}else{
					return { result: true }
				}          
			}
		}
	}

    return {
        addEventHandlers: addEventHandlers,
        getUserProfile:getUserProfile
    }
}
