var myPath = __dirname + "/dist";
module.exports = {
    mode : 'production',
    entry : {
        gstModel : 'js/main.js' 
    },
    output : {
        path : myPath,
        filename : 'bundle.js',
        library : gstModel
    }
}